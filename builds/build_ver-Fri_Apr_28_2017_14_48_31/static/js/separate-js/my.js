$(document).ready(function() {
	if($('#myChart-1').length) {
		// The speed gauge
		Highcharts.chart('myChart-1', {
			chart: {
				type: 'solidgauge'
			},

			title: null,

			pane: {
				center: ['50%', '85%'],
				size: '150%',
				startAngle: -90,
				endAngle: 90,
				background: {
					backgroundColor: '#eaeef1',
					innerRadius: '60%',
					outerRadius: '100%',
					shape: 'arc'
				}
			},

			tooltip: {
				enabled: false
			},

			// the value axis
			yAxis: {
				lineWidth: 0,
				minorTickInterval: null,
				tickAmount: 0,
				title: {
					y: -70
				},
				labels: {
					y: 16
				}
			},

			yAxis: {
				stops: [
					[0.1, '#4d80b4'],
					[1, '#4d80b4']
				],
				min: 0,
				max: 100,
				lineWidth: 0,
				minorTickInterval: null,
				tickAmount: 0,
				labels: {
					y: 16
				}
			},

			plotOptions: {
				solidgauge: {
					dataLabels: {
						y: 10,
						borderWidth: 0,
						style: {
							'fontFamily': 'Open Sans',
							'fontSize': '36px',
							'fontWeight': '300',
							'color': '#333',
						},
						useHTML: true,
					}
				}
			},

			credits: {
				enabled: false
			},

			series: [{
				name: 'Speed',
				data: [36],
				dataLabels: {
					format: '<div>{y}%</div>'
				},
				tooltip: {
					valueSuffix: ' %'
				}
			}]

		});
	}
	if($('#myChart-1-2').length) {
		Highcharts.chart('myChart-1-2', {
			chart: {
				type: 'column'
			},
			title: {
				text: 'NPS standx for Not Promote Score',
				align: 'left',
				verticalAlign: 'bottom',
				style: {
					'fontSize': '12px',
					'color': '#919191',
				}
			},
			subtitle: {
				text: false
			},
			credits: {
				enabled: false
			},
			xAxis: {
				categories: [
					'Mon',
					'Tue',
					'Wed',
					'Thu',
					'Fri',
					'Sat',
					'Today',
				],
				crosshair: false,
			},
			yAxis: {
				min: 0,
				title: {
					text: false
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.1,
					borderWidth: 0,
					showInLegend: false,
				},
			},
			series: [{
				name: 'NPS',
				data: [4, 5, 5, 2, 7, 6, 6],
				color:'#b9e36c',
			}]
		});
	}
	//END #myChart-1

	if($('#myChart-2').length) {
		Highcharts.chart('myChart-2', {
			chart: {
				type: 'bar'
			},
			title: {
				text: '<div style="padding-top: 5px">NPS standx for Not Promote Score</div>',
				align: 'left',
				useHTML: true,
				verticalAlign: 'bottom',
				style: {
					'fontSize': '12px',
					'color': '#919191',
				},
			},
			subtitle: {
				text: '<a href="#"><b>More Products</b></a>',
				align: 'right',
				useHTML: true,
				verticalAlign: 'bottom',
				style: {
					'fontSize': '12px',
					'color': '#4e80b5',
				},
			},
			credits: {
				enabled: false
			},
			xAxis: {
				categories: ['Product 1', 'Product 2', 'Product 3', 'Product 4'],
				title: {
					text: null
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: false,
					align: 'high'
				},
				labels: {
					overflow: 'justify'
				}
			},
			tooltip: {
				valueSuffix: '%'
			},
			plotOptions: {
				bar: {
					dataLabels: {
						enabled: true
					}
				}
			},
			legend: {
				enabled: false
			},
			credits: {
				enabled: false,
			},
			series: [{
				name: 'Smth',
				pointPadding: 0.2,
				data: [30, 70, 40, 50],
				color: '#4e80b5',
				dataLabels: {
					format: '{y}%',
					style: {
						'color' : '#919191',
						'fontWeight' : '300',
					}
				}
			}]
		});
	}
	//END #myChart-2

	if($('#myChart-3').length) {
		Highcharts.chart('myChart-3', {
			chart: {
				zoomType: 'xy'
			},
			title: {
				text: 'NPS standx for Not Promote Score',
				align: 'left',
				useHTML: true,
				verticalAlign: 'bottom',
				style: {
					'fontSize': '12px',
					'color': '#919191',
				},
			},
			subtitle: {
				text: false
			},
			credits: {
				enabled: false
			},
			xAxis: [{
				categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'Today', 'Mon', 'Tue'],
				crosshair: false
			}],
			yAxis: [{ // Primary yAxis
				labels: false,
				title: false
			}, { // Secondary yAxis
				labels: false,
				title: false
			}],
			tooltip: {
				shared: true
			},
			legend: {
				enabled: false
			},
			series: [{
				name: 'Something',
				type: 'column',
				color: '#b9e36c',
				pointPadding: 0.1,
				yAxis: 1,
				data: [3, 4, 5, 6, 7, 3, 5, 7, 5, 3],

			}, {
				name: 'Somthing+',
				type: 'spline',
				color: '#b9e36c',
				yAxis: 1,
				data: [4, 5, 6, 7, 7.5, 3.5, 5.5, 7.5, 6, 4],
			}]
		});
	}
	//END #myChart-3

	if($('#myChart-4').length) {
		Highcharts.chart('myChart-4', {
			chart: {
				type: 'bubble',
				plotBorderWidth: 0,
				zoomType: 'xy'
			},
			legend: {
				enabled: false
			},

			title: {
				text: 'Sugar and fat intake per country',
				align: 'left',
				useHTML: true,
				verticalAlign: 'bottom',
				style: {
					'fontSize': '12px',
					'color': '#919191',
				},
			},

			subtitle: {
				text: false
			},

			credits: {
				enabled: false
			},

			xAxis: {
				gridLineWidth: 0,
				title: {
					text: 'Daily fat intake'
				},
				labels: {
					format: '{value} gr'
				},
				plotLines: [{
					color: 'black',
					dashStyle: 'dot',
					width: 0,
					value: 65,
					zIndex: 3
				}]
			},

			yAxis: {
				startOnTick: false,
				endOnTick: false,
				title: {
					text: 'Daily sugar intake'
				},
				labels: {
					format: '{value} gr'
				},
				maxPadding: 0.2,
				plotLines: [{
					color: 'black',
					dashStyle: 'dot',
					width: 20,
					value: 50,
					label: {
						align: 'right',
						style: {
							fontStyle: 'italic'
						},
						text: 'Safe sugar intake 50g/day',
						x: -10
					},
					zIndex: 3
				}]
			},

			tooltip: {
				useHTML: true,
				headerFormat: '<table>',
				pointFormat: '<tr><th colspan="2"><h3>{point.country}</h3></th></tr>' +
					'<tr><th>Fat intake:</th><td>{point.x}g</td></tr>' +
					'<tr><th>Sugar intake:</th><td>{point.y}g</td></tr>' +
					'<tr><th>Obesity (adults):</th><td>{point.z}%</td></tr>',
				footerFormat: '</table>',
				followPointer: false
			},

			plotOptions: {
				series: {
					dataLabels: {
						enabled: true,
						format: '{point.name}',
						color : '#303030',
					}
				}
			},

			series: [{
				 data: [
					{
						name: '15%',
						x: 15,
						y: 1,
						z: 15,
						color: '#4e80b5',
						showInLegend: true
					},
					{
						name: '25%',
						x: 25,
						y: 1,
						z: 25,
						color: '#bfe574',
					},
					{
						name: '40%',
						x: 40,
						y: 1,
						z: 40,
						color: '#efd354',
					},
					{
						name: '70%',
						x: 70,
						y: 1,
						z: 70,
						color: '#d26565',
					},
				]
			}]

		});
	}
	//END #myChart-4

	if($('#myChart-5').length) {
		Highcharts.chart('myChart-5', {
			chart: {
				type: 'column'
			},
			title: {
				text: 'NPS standx for Not Promote Score',
				align: 'left',
				useHTML: true,
				verticalAlign: 'bottom',
				style: {
					'fontSize': '12px',
					'color': '#919191',
				},
			},
			subtitle: {
				text: false
			},
			credits: {
				enabled: false
			},
			xAxis: {
				categories: [
					'2016',
					'2017',
				],
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					enabled: false
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Lost Customers',
				data: [49.9, 71.5],
				color: '#4e80b5'

			}, {
				name: 'Expense',
				data: [83.6, 78.8],
				color: '#ebc865'

			}, {
				name: 'New Customers',
				data: [48.9, 38.8],
				color: '#bfe574'

			}, {
				name: 'Income',
				data: [42.4, 33.2],
				color: '#d26565'

			}]
		});
	}
	//END #myChart-5

	if($('#myChart-6').length) {
		Highcharts.chart('myChart-6', {

			chart: {
				type: 'heatmap',
				marginTop: 40,
				marginBottom: 80,
				plotBorderWidth: 1
			},

			title: {
				text: false
			},

			credits: {
				enabled: false
			},

			xAxis: {
				categories: ['Nirmala', 'Alif', 'Mario', 'Hanna', 'Gista', 'Dinda', 'Darsini', 'Sucipto', 'Adjie', 'Wendah']
			},

			yAxis: {
				categories: ['Fri', 'Thu', 'Wed', 'Tue', 'Mon'],
				title: null
			},

			colorAxis: {
				min: 0,
				minColor: '#FFFFFF',
				maxColor: Highcharts.getOptions().colors[0]
			},

			legend: {
				align: 'right',
				layout: 'vertical',
				margin: 0,
				verticalAlign: 'top',
				y: 25,
				symbolHeight: 280
			},

			tooltip: {
				formatter: function () {
					return '<b>' + this.series.xAxis.categories[this.point.x] + '</b> sold <br><b>' +
						this.point.value + '</b> items on <br><b>' + this.series.yAxis.categories[this.point.y] + '</b>';
				}
			},

			series: [{
				name: 'Sales per employee',
				borderWidth: 1,
				data: [[0, 0, 10], [0, 1, 19], [0, 2, 8], [0, 3, 24], [0, 4, 67], [1, 0, 92], [1, 1, 58], [1, 2, 78], [1, 3, 117], [1, 4, 48], [2, 0, 35], [2, 1, 15], [2, 2, 123], [2, 3, 64], [2, 4, 52], [3, 0, 72], [3, 1, 132], [3, 2, 114], [3, 3, 19], [3, 4, 16], [4, 0, 38], [4, 1, 5], [4, 2, 8], [4, 3, 117], [4, 4, 115], [5, 0, 88], [5, 1, 32], [5, 2, 12], [5, 3, 6], [5, 4, 120], [6, 0, 13], [6, 1, 44], [6, 2, 88], [6, 3, 98], [6, 4, 96], [7, 0, 31], [7, 1, 1], [7, 2, 82], [7, 3, 32], [7, 4, 30], [8, 0, 85], [8, 1, 97], [8, 2, 123], [8, 3, 64], [8, 4, 84], [9, 0, 47], [9, 1, 114], [9, 2, 31], [9, 3, 48], [9, 4, 91]],
				dataLabels: {
					enabled: true,
					color: '#000000'
				}
			}]

		});
	}
	//END #myChart-6

	if($('#myChart-7').length) {
		Highcharts.chart('myChart-7', {
			chart: {
				type: 'solidgauge',
			},

			title: {
				text: false,
			},

			credits: {
				enabled: false
			},

			legend: {
				useHTML: true,
				margin: 10,
				labelFormatter: function() {
					return '<div><span style="display:inline-block; margin-right:4px; padding:5px; background:' + this.data[0].color + '"></span>' + this.name + '</div>';
				},
				symbolWidth: 0
			},

			tooltip: {
				enabled: false
			},

			 pane: {
				startAngle: 0,
				endAngle: 360,
				background: [{ // Track for Move
					outerRadius: '112%',
					innerRadius: '88%',
					backgroundColor: '#f3f4f5',
					borderWidth: 0
				}]
			},

			yAxis: {
				min: 0,
				max: 100,
				lineWidth: 0,
				tickPositions: []
			},

			plotOptions: {
				solidgauge: {
					dataLabels: {
						y: -20,
						x: 2,
						borderWidth: 0,
						style: {
							'fontFamily': 'Open Sans',
							'fontSize': '24px',
							'fontWeight': '300',
							'color': '#333',
						},
						useHTML: true,
						format: '<div>60+</div>'
					},
					linecap: 'round',
					stickyTracking: false,
					rounded: true,
				}
			},

			series: [{
				name: 'Neutral',
				data: [{
					color: '#f3f4f5',
					radius: '112%',
					innerRadius: '88%',
					y: 60,
					
				}],
				showInLegend: true
			}, {
				name: 'Unhappy',
				data: [{
					color: '#c3b3b3',
					radius: '112%',
					innerRadius: '88%',
					y: 80
				}],
				showInLegend: true
			}, {
				name: 'Happy',
				data: [{
					color: '#d26565',
					radius: '112%',
					innerRadius: '88%',
					y: 60
				}],
				showInLegend: true
			}]
		});
	}
	//END #myChart-7

	if($('#myChart-7-2').length) {
		Highcharts.chart('myChart-7-2', {
			chart: {
				type: 'solidgauge',
			},

			title: {
				text: false,
			},

			credits: {
				enabled: false
			},

			legend: {
				useHTML: true,
				margin: 10,
				labelFormatter: function() {
					return '<div><span style="display:inline-block; margin-right:4px; padding:5px; background:' + this.data[0].color + '"></span>' + this.name + '</div>';
				},
				symbolWidth: 0
			},

			tooltip: {
				enabled: false
			},

			 pane: {
				startAngle: 0,
				endAngle: 360,
				background: [{ // Track for Move
					outerRadius: '112%',
					innerRadius: '88%',
					backgroundColor: '#f3f4f5',
					borderWidth: 0
				}]
			},

			yAxis: {
				min: 0,
				max: 100,
				lineWidth: 0,
				tickPositions: []
			},

			plotOptions: {
				solidgauge: {
					dataLabels: {
						y: -20,
						x: 2,
						borderWidth: 0,
						style: {
							'fontFamily': 'Open Sans',
							'fontSize': '24px',
							'fontWeight': '300',
							'color': '#333',
						},
						useHTML: true,
						format: '<div>40-59</div>'
					},
					linecap: 'round',
					stickyTracking: false,
					rounded: true
				}
			},

			series: [{
				name: 'Netural',
				data: [{
					color: '#f3f4f5',
					radius: '112%',
					innerRadius: '88%',
					y: 60
				}],
				showInLegend: true
			}, {
				name: 'Unhappy',
				data: [{
					color: '#ebf8c3',
					radius: '112%',
					innerRadius: '88%',
					y: 80
				}],
				showInLegend: true
			}, {
				name: 'Happy',
				data: [{
					color: '#b9e36c',
					radius: '112%',
					innerRadius: '88%',
					y: 60
				}],
				showInLegend: true
			}]
		});
	}
	//END #myChart-7-2

	if($('#myChart-7-3').length) {
		Highcharts.chart('myChart-7-3', {
			chart: {
				type: 'solidgauge',
			},

			title: {
				text: false,
			},

			credits: {
				enabled: false
			},

			legend: {
				useHTML: true,
				margin: 10,
				labelFormatter: function() {
					return '<div><span style="display:inline-block; margin-right:4px; padding:5px; background:' + this.data[0].color + '"></span>' + this.name + '</div>';
				},
				symbolWidth: 0
			},

			tooltip: {
				enabled: false
			},

			 pane: {
				startAngle: 0,
				endAngle: 360,
				background: [{ // Track for Move
					outerRadius: '112%',
					innerRadius: '88%',
					backgroundColor: '#f3f4f5',
					borderWidth: 0
				}]
			},

			yAxis: {
				min: 0,
				max: 100,
				lineWidth: 0,
				tickPositions: []
			},

			plotOptions: {
				solidgauge: {
					dataLabels: {
						y: -20,
						x: 2,
						borderWidth: 0,
						style: {
							'fontFamily': 'Open Sans',
							'fontSize': '24px',
							'fontWeight': '300',
							'color': '#333',
						},
						useHTML: true,
						format: '<div>25-40</div>'
					},
					linecap: 'round',
					stickyTracking: false,
					rounded: true
				}
			},

			series: [{
				name: 'Netural',
				data: [{
					color: '#f3f4f5',
					radius: '112%',
					innerRadius: '88%',
					y: 60
				}],
				showInLegend: true
			}, {
				name: 'Unhappy',
				data: [{
					color: '#f6f0c5',
					radius: '112%',
					innerRadius: '88%',
					y: 80
				}],
				showInLegend: true
			}, {
				name: 'Happy',
				data: [{
					color: '#efd354',
					radius: '112%',
					innerRadius: '88%',
					y: 60
				}],
				showInLegend: true
			}]
		});
	}
	//END #myChart-7-3

	if($('#myChart-7-4').length) {
		Highcharts.chart('myChart-7-4', {
			chart: {
				type: 'solidgauge',
			},

			title: {
				text: false,
			},

			credits: {
				enabled: false
			},

			legend: {
				useHTML: true,
				margin: 10,
				labelFormatter: function() {
					return '<div><span style="display:inline-block; margin-right:4px; padding:5px; background:' + this.data[0].color + '"></span>' + this.name + '</div>';
				},
				symbolWidth: 0
			},

			tooltip: {
				enabled: false
			},

			 pane: {
				startAngle: 0,
				endAngle: 360,
				background: [{ // Track for Move
					outerRadius: '112%',
					innerRadius: '88%',
					backgroundColor: '#f3f4f5',
					borderWidth: 0
				}]
			},

			yAxis: {
				min: 0,
				max: 100,
				lineWidth: 0,
				tickPositions: []
			},

			plotOptions: {
				solidgauge: {
					dataLabels: {
						y: -20,
						x: 2,
						borderWidth: 0,
						style: {
							'fontFamily': 'Open Sans',
							'fontSize': '24px',
							'fontWeight': '300',
							'color': '#333',
						},
						useHTML: true,
						format: '<div>0-24</div>'
					},
					linecap: 'round',
					stickyTracking: false,
					rounded: true
				}
			},

			series: [{
				name: 'Netural',
				data: [{
					color: '#f3f4f5',
					radius: '112%',
					innerRadius: '88%',
					y: 60
				}],
				showInLegend: true
			}, {
				name: 'Unhappy',
				data: [{
					color: '#b3d9db',
					radius: '112%',
					innerRadius: '88%',
					y: 80
				}],
				showInLegend: true
			}, {
				name: 'Happy',
				data: [{
					color: '#4e80b5',
					radius: '112%',
					innerRadius: '88%',
					y: 60
				}],
				showInLegend: true
			}]
		});
	}
	//END #myChart-7-4

	if($('#myChart-8').length) {
		// Age categories
		var categories = ['0-24', '25-39', '40-59', '60 + '];
		Highcharts.chart('myChart-8', {
			chart: {
				type: 'bar'
			},
			title: {
				text: false
			},
			subtitle: {
				text: false
			},
			xAxis: [{
				categories: categories,
				reversed: false,
				labels: {
					step: 1
				}
			}, { // mirror axis on right side
				opposite: true,
				reversed: false,
				categories: categories,
				linkedTo: 0,
				labels: {
					step: 1
				}
			}],
			yAxis: {
				title: {
					text: null
				},
				labels: {
					formatter: function () {
						return Math.abs(this.value) + '%';
					}
				}
			},

			plotOptions: {
				series: {
					stacking: 'normal'
				}
			},

			tooltip: {
				formatter: function () {
					return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
						'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
				}
			},

			series: [{
				name: 'Male',
				data: [-52, -70, -30, -36],
				color: '#4e80b5'
			}, {
				name: 'Female',
				data: [30, 24, 52, 49],
				color: '#b9e36c'
			}]
		});
	}
	//END #myChart-7-3
});