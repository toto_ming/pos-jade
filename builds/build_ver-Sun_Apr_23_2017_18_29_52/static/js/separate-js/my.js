$(document).ready(function() {
	for (var i = 1; i < 9; i++) {
		if($('#myChart-'+i).length) {
			var myChart = echarts.init(document.getElementById('myChart-'+i));

			var option = {
				tooltip: {
					show: true
				},
				legend: {
					data:['Sales', 'Hello']
				},
				xAxis : [
					{
						type : 'category',
						data : ["Shirts", "Sweaters", "Chiffon Shirts", "Pants", "High Heels", "Socks"]
					}
				],
				yAxis : [
					{
						type : 'value'
					}
				],
				series : [
					{
						"name":"Sales",
						"type":"bar",
						"data":[5, 20, 40, 10, 10, 20]
					}
				]
			};
			myChart.setOption(option); 
		}
	}
});