'use strict';

/*
    This file can be used as entry point for webpack!
 */

// Data Picker Initialization
if($('.datepicker').length) {
    $('.datepicker').pickadate();
}

// Time Picker Initialization
$('.timepicker').pickatime({
    twelvehour: true
});
$('.timepicker-darktheme').pickatime({
    twelvehour: true,
    darktheme: true
});

// SideNav init
if($('.custom-scrollbar').length) {
    $(".button-collapse").sideNav();
    var el = document.querySelector('.custom-scrollbar');
    Ps.initialize(el);
}

if($('.md-form .mdb-select').length) {
    $('.md-form .mdb-select').material_select();
}