var data = {
    header: {
        menu: [
            {
                url: 'dashboard.html',
                title: 'Dashboard',
                icon: 'fa-dashboard'
            },
            {
                url: '#',
                title: 'Sales',
                icon: 'fa-tag',
                sub: [
                    {
                        url: 'add-invoice.html',
                        title: 'Add invoice',
                    },
                    {
                        url: 'view-invoice.html',
                        title: 'View invoice',
                    },
                    {
                        url: 'orders.html',
                        title: 'Orders',
                    },
                    {
                        url: 'add-product.html',
                        title: 'Add product',
                    },
                    {
                        url: 'pos-1.html',
                        title: 'POS',
                    },
                ]
            },
            {
                url: '#',
                title: 'UI Features',
                icon: 'fa-pencil-square-o',
                sub: [
                    {
                        url: 'bootstrap-grid.html',
                        title: 'Bootstrap grid',
                    },
                    {
                        url: 'colors.html',
                        title: 'Colors',
                    },
                    {
                        url: 'icons-list.html',
                        title: 'Icons list',
                    },
                    {
                        url: 'alerts.html',
                        title: 'Alerts',
                    },
                    {
                        url: 'modals.html',
                        title: 'Modals',
                    },
                    {
                        url: 'buttons.html',
                        title: 'Buttons',
                    },
                    {
                        url: 'social-buttons.html',
                        title: 'Social buttons',
                    },
                    {
                        url: 'typography.html',
                        title: 'Typography',
                    },
                    {
                        url: 'tabs-pills.html',
                        title: 'Tabs & Pills',
                    },
                    {
                        url: 'tags-badges-labels.html',
                        title: 'Tags, Badges & Labels',
                    },
                    {
                        url: 'progress-bars.html',
                        title: 'Progress Bars',
                    },
                    {
                        url: 'waves-effect.html',
                        title: 'Waves effect',
                    },
                    {
                        url: 'tooltips.html',
                        title: 'Tooltips',
                    },
                    {
                        url: 'panels.html',
                        title: 'Panels',
                    },
                    {
                        url: 'material-select.html',
                        title: 'Material select',
                    },
                ]
            },
            {
                url: '#',
                title: 'Components',
                icon: 'fa-cubes',
                sub: [
                    {
                        url: 'date-picker.html',
                        title: 'Date picker',
                    },
                    {
                        url: 'time-picker.html',
                        title: 'Time picker',
                    },
                    {
                        url: 'inputs.html',
                        title: 'Inputs',
                    },
                    {
                        url: 'forms.html',
                        title: 'Forms',
                    },
                    {
                        url: 'dropdowns.html',
                        title: 'Dropdowns',
                    },
                    {
                        url: 'popovers.html',
                        title: 'Popovers',
                    },
                    {
                        url: 'footers.html',
                        title: 'Footers',
                    },
                    {
                        url: 'pagination.html',
                        title: 'Pagination',
                    },
                ]
            },
            {
                url: 'tables.html',
                title: 'Tables',
                icon: 'fa-table'
            },
            {
                url: '#',
                title: 'Charts',
                icon: 'fa-pie-chart',
                sub: [
                    {
                        url: 'highcharts.html',
                        title: 'Highcharts',
                    },
                    {
                        url: 'amcharts.html',
                        title: 'amCharts',
                    },
                    {
                        url: 'bootstrap-charts.html',
                        title: 'Bootstrap charts',
                    },
                ]
            },
            {
                url: '#',
                title: 'Pages',
                icon: 'fa-file',
                sub: [
                    {
                        url: 'login.html',
                        title: 'Login',
                    },
                    {
                        url: 'register.html',
                        title: 'Register',
                    },
                ]
            }
        ]
    }
};